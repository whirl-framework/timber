#pragma once

#include <timber/log/record.hpp>

namespace timber::log {

struct IBackend {
  virtual ~IBackend() = default;

  virtual Level GetMinLevelFor(const std::string& component) const = 0;

  // Capture current event context (e.g. tracing context)
  virtual ExtraFields CurrentSiteContext() {
    return {};
  }

  virtual void Log(Record record) = 0;
};

}  // namespace timber::log
