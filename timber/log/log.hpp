#pragma once

#include <timber/log/logger.hpp>

#include <fmt/core.h>
// operator << is enough for logging
#include <fmt/ostream.h>

//////////////////////////////////////////////////////////////////////

#define _TIMBER_LOG_IMPL(level, ...)                \
  do {                                              \
    if (logger_.IsLevelEnabled(level)) {            \
      logger_.Log(level, fmt::format(__VA_ARGS__)); \
    }                                               \
  } while (false)

//////////////////////////////////////////////////////////////////////

#define TIMBER_LOG(level, ...) _TIMBER_LOG_IMPL(level, __VA_ARGS__)

//////////////////////////////////////////////////////////////////////

#define TIMBER_LOG_TRACE(...) _TIMBER_LOG_IMPL(timber::log::Level::Trace, __VA_ARGS__)
#define TIMBER_LOG_DEBUG(...) _TIMBER_LOG_IMPL(timber::log::Level::Debug, __VA_ARGS__)
#define TIMBER_LOG_INFO(...) _TIMBER_LOG_IMPL(timber::log::Level::Info, __VA_ARGS__)
#define TIMBER_LOG_WARN(...) _TIMBER_LOG_IMPL(timber::log::Level::Warning, __VA_ARGS__)
#define TIMBER_LOG_ERROR(...) _TIMBER_LOG_IMPL(timber::log::Level::Error, __VA_ARGS__)
#define TIMBER_LOG_CRITICAL(...) _TIMBER_LOG_IMPL(timber::log::Level::Critical, __VA_ARGS__)

//////////////////////////////////////////////////////////////////////
