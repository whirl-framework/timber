#include <timber/log/logger.hpp>

#include <compass/locate.hpp>

namespace timber::log {

Logger::Logger(std::string_view component)
  : Logger(component, compass::Locate<IBackend>()) {
}

}  // namespace timber::log
